﻿using UnityEngine;
using System.Collections;

namespace SofaWarriors.Parameters
{

    public class EnemyParameters : ScriptableObject
    {
#pragma warning disable
        [SerializeField]
        UnitPassport warrior;
        [SerializeField]
        UnitPassport archer;
        [SerializeField]
        UnitPassport boss;

        [SerializeField]
        float warriorChance;
        [SerializeField]
        float archerChance;
        [SerializeField]
        float bossChance;
#pragma warning restore

        public UnitPassport WarriorPassport
        {
            get { return warrior; }
        }
        public UnitPassport ArcherPassport
        {
            get { return archer; }
        }
        public UnitPassport BossPassport
        {
            get { return boss; }
        }

        public float WarriorChance
        {
            get { return warriorChance; }
        }
        public float ArcherChance
        {
            get { return archerChance; }
        }
        public float BossChance
        {
            get { return bossChance; }
        }

    }
}