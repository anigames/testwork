﻿using UnityEngine;
using System.Collections;
using System;

namespace SofaWarriors
{

    public class Minion : UnitBase, ISelectable
    {
#pragma warning disable
        [SerializeField]
        private Vector3 fountainPosition;

        [SerializeField]
        private GameObject selectionMarker;
#pragma warning restore

        private bool isSelected = false;

        private Coroutine attackingCoroutine;

        protected override void PrepareUnit()
        {
            objectController.OnEnemyAdd += delegate 
            {
                if (TargetUnit==null && State != UnitState.Dying)
                    SetState(UnitState.LookingEnemy);
            };

            SetState(UnitState.LookingEnemy);
        }

        public override void SetState(UnitState state)
        {
            if (State == UnitState.Dying)
            {
                Debug.Log("asd " + state);
                return;
            }

            switch (state)
            {
                case UnitState.Idle:
                    animator.Play("idle");
                    rigid.isKinematic = true;
                    break;

                case UnitState.LookingFountain:
                    animator.Play("walk");
                    rigid.isKinematic = false;
                    break;

                case UnitState.RecoveryHealth:
                    rigid.velocity = Vector3.zero;
                    animator.Play("idle");
                    rigid.isKinematic = true;
                    break;

                case UnitState.LookingEnemy:
                    //try to find enemy. if has no enemies go to Fountain
                    FindNearestEnemy();
                    if (TargetUnit == null)
                    {
                        SetState(UnitState.LookingFountain);
                        return;
                    }

                    animator.Play("walk");
                    rigid.isKinematic = false;
                    
                    break;

                case UnitState.Attack:
                    animator.Play("idle");
                    rigid.velocity = Vector3.zero;
                    rigid.isKinematic = true;
                    attackingCoroutine = StartCoroutine(Attacking());
                    break;

                case UnitState.Dying:
                    if (attackingCoroutine != null)
                        StopCoroutine(attackingCoroutine);
                    rigid.isKinematic = true;
                    objectController.RemoveUnit(this, WorldObjectType.Minion);
                    Invoke("BackToPool", 1);
                    animator.Play("dying");
                    break;
            }

            State = state;
        }
        
        public override void UpdateUnit()
        {
            switch (State)
            {
                case UnitState.LookingFountain:
                    if ((this.transform.position - fountainPosition).magnitude > 0.35f)
                    {
                        MoveTo(fountainPosition);
                    }
                    else
                    {                        
                        SetState(UnitState.RecoveryHealth);
                    }
                    break;

                case UnitState.RecoveryHealth:
                    if ((this.transform.position - fountainPosition).magnitude > 0.5f)
                    {
                        SetState(UnitState.LookingFountain);
                    }
                    break;

                case UnitState.LookingEnemy:
                    if (TargetUnit==null || TargetUnit.Health <= 0)
                        SetState(UnitState.LookingEnemy);

                    if ((transform.position - TargetUnit.transform.position).magnitude >= unitPassport.AttackRange)
                    {
                        MoveTo(TargetUnit.transform.position);
                    }
                    else
                    {
                        SetState(UnitState.Attack);
                    }
                    break;
            }
        }



        void Update()
        {
            UpdateUnit();
        }

        IEnumerator Attacking()
        {
            while (TargetUnit.Health > 0 && (TargetUnit.transform.position - transform.position).magnitude <= unitPassport.AttackRange)
            {                
                animator.Play("attack");
                TargetUnit.Damage(unitPassport.Attack, this);
                yield return new WaitForSeconds(1 / unitPassport.AttackSpeed);
            }
            SetState(UnitState.LookingEnemy);
        }



        #region ISelectable
        public bool IsSelected()
        {
            return isSelected;
        }

        public void Select()
        {
            isSelected = true;
            selectionMarker.SetActive(true);
        }

        public void Unselect()
        {
            isSelected = false;
            selectionMarker.SetActive(false);
        }

        #endregion

    }
}
