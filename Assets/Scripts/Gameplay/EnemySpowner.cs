﻿using UnityEngine;
using System.Collections;
using SofaWarriors.Parameters;

namespace SofaWarriors
{

    public class EnemySpowner : Building
    {

        [SerializeField]
        private float spownDelay = 3;

        //Enemy declaration
        private EnemyParameters enemyParameters;
        private ObjectFactory objectFactory;
        private ObjectController objectController;

#pragma warning disable
        //Local spown points from position of Television
        Vector3 spownPosition = Vector3.forward * -1;
#pragma warning restore

        void Start()
        {
            objectFactory = GameManager.Instance.GetObjectFactory();
            objectController = GameManager.Instance.GetObjectController();
            enemyParameters = Resources.Load<EnemyParameters>("enemy_passports");
            StartCoroutine(SpownEnemy());
        }

        IEnumerator SpownEnemy()
        {
            int waves = 0;

            while (waves < 5)
            {
                int enemyNum = 0;
                while (enemyNum < 10)
                {
                    yield return new WaitForSeconds(spownDelay);
                    Spown();
                }
                yield return new WaitForSeconds(7);
            }
            GameManager.Instance.State = GameState.Win;
        }

        void Spown()
        {

            float archerChance = Random.Range(0, enemyParameters.ArcherChance);
            float warriorChance = Random.Range(0, enemyParameters.WarriorChance);
            float bossChance = Random.Range(0, enemyParameters.BossChance);

            UnitPassport passport;
            if (archerChance > warriorChance)
            {
                if (archerChance > bossChance)
                    passport = enemyParameters.ArcherPassport;
                else
                    passport = enemyParameters.BossPassport;
            }
            else
            {
                if (warriorChance > bossChance)
                    passport = enemyParameters.WarriorPassport;
                else
                    passport = enemyParameters.BossPassport;
            }

            var unit = (Enemy)objectFactory.CreateUnit(passport, transform.position + spownPosition + Vector3.right*Random.Range(-2f,2f));
            objectController.AddEnemy(unit);
        }

    }
}
