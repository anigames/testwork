﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace SofaWarriors.UI
{

    public class UIStarting : MonoBehaviour
    {

        [SerializeField]
        Text timer;

        void Start()
        {
            GameManager.OnStartingTimerTick += GameManager_OnStartingTimerTick;
        }

        void OnDestroy()
        {
            GameManager.OnStartingTimerTick -= GameManager_OnStartingTimerTick;
        }

        private void GameManager_OnStartingTimerTick(int timeLeft)
        {
            timer.text = (timeLeft-1).ToString();
        }
    }
}
