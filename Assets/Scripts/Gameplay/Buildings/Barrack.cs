﻿using UnityEngine;
using System.Collections;
using SofaWarriors.Parameters;

namespace SofaWarriors
{

    public class Barrack : Building
    {
        public static event System.Action<UnitBase> OnMinionAdded;

#pragma warning disable
        [SerializeField]
        private UnitType unitType;
        [SerializeField]
        private Vector3 localSpawnPoint;
#pragma warning restore

        private ObjectFactory factory;
        private ObjectController objectController;
        private UnitPassport minionPassport;

        void Start()
        {
            factory = GameManager.Instance.GetObjectFactory();
            objectController = GameManager.Instance.GetObjectController();

            var minionParameters = Resources.Load<MinionParameters>("minion_passports");
            minionPassport = minionParameters.GetPassport(unitType);

            StartCoroutine(AddMinion());
        }

        IEnumerator AddMinion()
        {
            while (true)
            {
                yield return new WaitForSeconds(5);
                var unit = (Minion)factory.CreateUnit(minionPassport, this.transform.position + localSpawnPoint);
                objectController.AddMinion(unit);
            }
        }
    }
}