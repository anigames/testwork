﻿using UnityEngine;
using System.Collections;

namespace SofaWarriors
{
    public class Frustum
    {
        public Vector3 X { get; set; }
        public Vector3 Y { get; set; }
        public Vector3 Z { get; set; }
        public Vector3 W { get; set; }

        public Vector3 A { get; set; }
        public Vector3 B { get; set; }
        public Vector3 C { get; set; }
        public Vector3 D { get; set; }

        public Frustum(Vector3 x, Vector3 y, Vector3 z, Vector3 w, Vector3 a, Vector3 b, Vector3 c, Vector3 d)
        {
            X = x;
            Y = y;
            Z = z;
            W = w;

            A = a;
            B = b;
            C = c;
            D = d;
        }

        public bool Contains(Vector3 pos)
        {
            bool inside_up = IsInside(X, A - X, Y - X, pos);
            bool inside_left = IsInside(W, D - W, X - W, pos);
            bool inside_right = IsInside(Y, B - Y, Z - Y, pos);
            bool inside_down = IsInside(Z, C - Z, W - Z, pos);

            return inside_up && inside_left && inside_right && inside_down;
        }

        bool IsInside(Vector3 origin, Vector3 forward, Vector3 right, Vector3 pos)
        {
            Vector3 normal = Vector3.Cross(forward, right);
            return Vector3.Dot(normal, pos - origin) < 0;
        }

    }
}
