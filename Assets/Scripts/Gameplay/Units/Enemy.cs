﻿using UnityEngine;
using System.Collections;
using System;

namespace SofaWarriors
{

    public class Enemy : UnitBase
    {
        private Coroutine attackingCoroutine;

        protected override void PrepareUnit()
        {
            SetState(UnitState.LookingEnemy);
        }

        public override void SetState(UnitState state)
        {
            if (State == UnitState.Dying)
                Debug.Log("asd");

            switch (state)
            {
                case UnitState.Idle:
                    animator.Play("idle");
                    rigid.isKinematic = true;
                    break;

                case UnitState.LookingEnemy:
                    //try to find enemy. if has no enemies go to attack Sofa
                    FindNearestEnemy();
                    if (TargetUnit == null || (TargetUnit.transform.position - transform.position).magnitude>2 )
                    {
                        TargetUnit = objectController.Sofa;
                    }

                    animator.Play("walk");
                    rigid.isKinematic = false;

                    break;

                case UnitState.Attack:
                    animator.Play("idle");
                    rigid.velocity = Vector3.zero;
                    rigid.isKinematic = true;
                    attackingCoroutine = StartCoroutine(Attacking());
                    break;

                case UnitState.Dying:
                    if (attackingCoroutine != null)
                        StopCoroutine(attackingCoroutine);
                    rigid.isKinematic = true;
                    objectController.RemoveUnit(this, WorldObjectType.Enemy);
                    Invoke("BackToPool", 1);
                    animator.Play("dying");
                    break;
            }

            State = state;
        }

        public override void UpdateUnit()
        {
            switch (State)
            {

                case UnitState.LookingEnemy:
                    if (TargetUnit.Health <= 0)
                        SetState(UnitState.LookingEnemy);

                    if ((transform.position - TargetUnit.transform.position).magnitude >= unitPassport.AttackRange)
                    {
                        MoveTo(TargetUnit.transform.position);
                    }
                    else
                    {
                        SetState(UnitState.Attack);
                    }
                    break;
            }
        }

        void Update()
        {
            UpdateUnit();
        }

        IEnumerator Attacking()
        {
            while (TargetUnit!=null && TargetUnit.Health > 0 && (TargetUnit.transform.position - transform.position).magnitude <= unitPassport.AttackRange)
            {                
                animator.Play("attack");
                TargetUnit.Damage(unitPassport.Attack, this);
                yield return new WaitForSeconds(1 / unitPassport.AttackSpeed);
            }
            SetState(UnitState.LookingEnemy);
        }
    }
}