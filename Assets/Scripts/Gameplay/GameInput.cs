﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace SofaWarriors
{
   //Класс GameInput работает автономно.
   //Взаимодействие с ним происходит засчет
   //подписывания на события OnWorldSelected и OnWorldClicked,
   //которые передают пирамиду Frustum, которая описывает область
   //нажатия или выделения

    public class GameInput : MonoBehaviour
    {
        public static event System.Action<Frustum> OnWorldSelected;
        public static event System.Action<Frustum> OnWorldClicked;

        Vector3 selectionStart;
        Vector3 selectionEnd;

#pragma warning disable
        [SerializeField]
        Image selectionFrame;
        
#pragma warning restore

        Frustum frustum;

        float clickLimit = 0.1f;
        float frustumLen = 50f;

        void Awake()
        {
            selectionFrame.gameObject.SetActive(false);
        }

        void Update()
        {
            //OnMouseDown
            if (Input.GetMouseButtonDown(0))
            {
                selectionStart = Vector3.zero;
                selectionFrame.gameObject.SetActive(true);
            }

            //OnMousePress
            if (Input.GetMouseButton(0))
            {

                if (selectionStart == Vector3.zero)
                    selectionStart = Input.mousePosition;

                selectionEnd = Input.mousePosition;

                var minAnchor = new Vector2(Mathf.Min(selectionStart.x, selectionEnd.x) / Screen.width,
                    Mathf.Min(selectionStart.y, selectionEnd.y) / Screen.height);
                var maxAnchor = new Vector2(Mathf.Max(selectionStart.x, selectionEnd.x) / Screen.width,
                    Mathf.Max(selectionStart.y, selectionEnd.y) / Screen.height);

                selectionFrame.rectTransform.anchorMin = minAnchor;
                selectionFrame.rectTransform.anchorMax = maxAnchor;
                
            }

            //OnMouseUp
            if (Input.GetMouseButtonUp(0))
            {

                var inputMode = InputMode.Selection;

                if ((selectionEnd - selectionStart).magnitude < clickLimit*Screen.width)
                {
                    Vector3 average = (selectionEnd + selectionStart) / 2;
                    selectionEnd = average - Vector3.right * clickLimit * Screen.width * 0.5f - Vector3.up * clickLimit * Screen.width * 0.5f;
                    selectionStart = average + Vector3.right * clickLimit * Screen.width * 0.5f + Vector3.up * clickLimit * Screen.width * 0.5f;
                    inputMode = InputMode.Click;
                }

                var camX = new Vector3(Mathf.Min(selectionStart.x, selectionEnd.x), Mathf.Max(selectionStart.y, selectionEnd.y), 0);
                var camY = new Vector3(Mathf.Max(selectionStart.x, selectionEnd.x), Mathf.Max(selectionStart.y, selectionEnd.y), 0);
                var camZ = new Vector3(Mathf.Max(selectionStart.x, selectionEnd.x), Mathf.Min(selectionStart.y, selectionEnd.y), 0);
                var camW = new Vector3(Mathf.Min(selectionStart.x, selectionEnd.x), Mathf.Min(selectionStart.y, selectionEnd.y), 0);                

                Ray rayX = Camera.main.ScreenPointToRay(camX);
                Ray rayY = Camera.main.ScreenPointToRay(camY);
                Ray rayZ = Camera.main.ScreenPointToRay(camZ);
                Ray rayW = Camera.main.ScreenPointToRay(camW);

                var worldX = rayX.origin;
                var worldY = rayY.origin;
                var worldZ = rayZ.origin;
                var worldW = rayW.origin;

                var worldA = rayX.origin + rayX.direction * frustumLen;
                var worldB = rayY.origin + rayY.direction * frustumLen;
                var worldC = rayZ.origin + rayZ.direction * frustumLen;
                var worldD = rayW.origin + rayW.direction * frustumLen;

                frustum = new Frustum(worldX, worldY, worldZ, worldW, worldA, worldB, worldC, worldD);

                switch (inputMode)
                {
                    case InputMode.Click:
                        if (OnWorldClicked != null)
                            OnWorldClicked(frustum);
                        break;
                    case InputMode.Selection:
                        if (OnWorldSelected != null)
                            OnWorldSelected(frustum);
                        break;
                }
                

                selectionFrame.gameObject.SetActive(false);
            }

        }

        void OnDrawGizmos()
        {
            Gizmos.color = new Color(0.9f, 1, 0.5f, 0.25f);
            if (frustum != null)
            {
                Gizmos.DrawLine(frustum.X, frustum.A);
                Gizmos.DrawLine(frustum.Y, frustum.B);
                Gizmos.DrawLine(frustum.Z, frustum.C);
                Gizmos.DrawLine(frustum.W, frustum.D);
                
                Gizmos.DrawLine(frustum.X, frustum.Y);
                Gizmos.DrawLine(frustum.Y, frustum.Z);
                Gizmos.DrawLine(frustum.Z, frustum.W);
                Gizmos.DrawLine(frustum.W, frustum.X);

                Gizmos.DrawLine(frustum.A, frustum.B);
                Gizmos.DrawLine(frustum.B, frustum.C);
                Gizmos.DrawLine(frustum.C, frustum.D);
                Gizmos.DrawLine(frustum.D, frustum.A);
            }

        }


    }
}
