﻿using UnityEngine;
using System.Collections;

namespace SofaWarriors
{

    public class BuildingParameters : ScriptableObject
    {
        [SerializeField]
        private BuildingPassport sofa;
        [SerializeField]
        private BuildingPassport warriorBarrack;
        [SerializeField]
        private BuildingPassport archerBarrack;
        [SerializeField]
        private BuildingPassport fountain;
        [SerializeField]
        private BuildingPassport television;

        public BuildingPassport Sofa
        {
            get { return sofa; }
        }

        public BuildingPassport ArcherBarrack
        {
            get { return archerBarrack; }
        }

        public BuildingPassport WarriorBarrack
        {
            get { return warriorBarrack; }
        }

        public BuildingPassport Fountain
        {
            get { return fountain; }
        }

        public BuildingPassport Television
        {
            get { return television; }
        }

        public BuildingPassport GetBarrack(UnitType unitType)
        {
            switch (unitType)
            {
                case UnitType.Archer:
                    return archerBarrack;
                default:
                    return warriorBarrack;
            }
        }

        public BuildingPassport[] GetAllBildings()
        {
            return new BuildingPassport[] { warriorBarrack, archerBarrack, fountain, television };
        }

    }
}