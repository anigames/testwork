﻿using UnityEngine;
using System.Collections;

namespace SofaWarriors.Parameters
{

    public class MinionParameters : ScriptableObject
    {
#pragma warning disable
        [SerializeField]
        UnitPassport warrior;
        [SerializeField]
        UnitPassport archer;
        [SerializeField]
        UnitPassport player;
#pragma warning restore

        public UnitPassport WarriorPassport { get { return warrior; } }
        public UnitPassport ArcherPassport { get { return archer; } }
        public UnitPassport PlayerPassport { get { return player; } }

        public UnitPassport GetPassport(UnitType type)
        {
            switch (type)
            {
                case UnitType.Archer:
                    return archer;
                case UnitType.Warrior:
                    return warrior;
                case UnitType.Player:
                    return player;
                default:
                    return warrior;
            }
        }

    }
}