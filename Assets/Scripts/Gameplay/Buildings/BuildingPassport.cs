﻿using UnityEngine;
using System.Collections;
namespace SofaWarriors
{
    [System.Serializable]
    public class BuildingPassport
    {

        [SerializeField]
        private string name;
        [SerializeField]
        private GameObject prefab;
        [SerializeField]
        private Sprite icon;

        public string Name
        {
            get { return name; }
        }
        public GameObject Prefab
        {
            get { return prefab; }
        }
        public Sprite Icon
        {
            get { return icon; }
        }


    }
}