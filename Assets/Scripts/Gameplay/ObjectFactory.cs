﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SofaWarriors
{

    public class ObjectFactory : MonoBehaviour
    {

        private List<GameObject> objectList;

        void Awake()
        {
            objectList = new List<GameObject>();
        }

        public GameObject Build(GameObject obj, Vector3? pos, Vector3? euler)
        {
            var newObject = Instantiate(obj) as GameObject;
            newObject.name = obj.name;

            if(pos!=null)
                newObject.transform.position = pos.Value;
            if(euler!=null)
                newObject.transform.eulerAngles = euler.Value;

            newObject.transform.SetParent(this.transform);

            objectList.Add(newObject);

            return newObject;
        }

        public GameObject Build(GameObject obj)
        {
            return Build(obj, null, null);
        }

        public UnitBase CreateUnit(UnitPassport passport, Vector3 position)
        {
            GameObject obj = Build(passport.Prefab, position, Vector3.zero);
            UnitBase unit = obj.GetComponent<UnitBase>();
            unit.Initialize(passport,GameManager.Instance.GetObjectController());
            return unit;
        }

        public void ClearAll()
        {
            foreach (GameObject obj in objectList)
            {
                Destroy(obj);
            }
            objectList.Clear();
            System.GC.Collect();
        }
    }
}