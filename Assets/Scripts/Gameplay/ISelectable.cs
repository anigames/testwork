﻿using UnityEngine;
using System.Collections;

public interface ISelectable
{
    void Select();
    void Unselect();
    bool IsSelected();
}
