﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SofaWarriors
{

    public class ObjectController
    {
        public static event System.Action<Building> OnBuildingSelect;
        public static event System.Action OnUnselect;

        public event System.Action<int> OnBuildCreated;

        public event System.Action OnEnemyAdd;

        List<UnitBase> minions;
        List<UnitBase> enemies;
        List<Building> buildings;
        SofaUnit sofa;

        public SofaUnit Sofa { get { return sofa; } }

        bool hasSelectedMinion = false;

        public ObjectController()
        {
            minions = new List<UnitBase>();
            enemies = new List<UnitBase>();
            buildings = new List<Building>();

            GameInput.OnWorldSelected += OnSelected;
            GameInput.OnWorldClicked += OnClicked;
        }

        ~ObjectController()
        {
            GameInput.OnWorldSelected -= OnSelected;
            GameInput.OnWorldClicked -= OnClicked;
        }

        public void AddMinion(Minion minion)
        {
            minions.Add(minion);
        }

        public void AddEnemy(Enemy enemy)
        {
            enemies.Add(enemy);
            if (OnEnemyAdd != null)
                OnEnemyAdd();
        }

        public void AddBuilding(Building building)
        {
            buildings.Add(building);
            if (OnBuildCreated!=null)
                OnBuildCreated(buildings.Count);
        }


        public void AddObjectByType(GameObject obj, WorldObjectType type)
        {
            switch (type)
            {
                case WorldObjectType.Building:
                    AddBuilding(obj.GetComponent<Building>());
                    break;
                case WorldObjectType.Minion:
                    AddMinion(obj.GetComponent<Minion>());
                    break;
                case WorldObjectType.Enemy:
                    AddEnemy(obj.GetComponent<Enemy>());
                    break;
            }
        }

        public UnitBase GetNearestEnemy(UnitBase unit, WorldObjectType type)
        {
            if (type == WorldObjectType.Building)
            {
                Debug.LogError("Building is not Unit!");
                return null;
            }

            List<UnitBase> units;
            if (type == WorldObjectType.Minion)
                units = enemies;
            else
                units = minions;

            if (units.Count == 0)
                return null;

            int numNearest = 0;
            float minDistance = float.MaxValue;

            for (int i = 0; i < units.Count; i++)
            {
                if ((unit.transform.position - units[i].transform.position).magnitude < minDistance)
                    numNearest = i;
            }

            return units[numNearest];

        }


        void OnSelected(Frustum frustum)
        {
            if (OnUnselect!=null)
                OnUnselect();

            CheckMinions(frustum);

            foreach (var building in buildings)
            {
                building.Unselect();
            }
        }

        void OnClicked(Frustum frustum)
        {
            if (OnUnselect != null)
                OnUnselect();

            var isBuildingSeleceted = false;

            foreach (var building in buildings)
            {
                if (frustum.Contains(building.transform.position) && !hasSelectedMinion)
                {
                    building.Select();
                    isBuildingSeleceted = true;
                    if (OnBuildingSelect != null)
                        OnBuildingSelect(building);
                }
                else
                    building.Unselect();
            }

            if (isBuildingSeleceted)
                return;

            CheckMinions(frustum);
        }

        void CheckMinions(Frustum frustum)
        {
            hasSelectedMinion = false;
            foreach (var unit in minions)
            {
                if (frustum.Contains(unit.transform.position))
                {
                    (unit as ISelectable).Select();
                    hasSelectedMinion = true;
                }
                else
                    (unit as ISelectable).Unselect();
            }
        }

        public int GetEnemyCount()
        {
            return enemies.Count;
        }

        public void RemoveUnit(UnitBase unit, WorldObjectType type)
        {
            switch (type)
            {
                case WorldObjectType.Enemy:
                    enemies.Remove(unit);
                    break;
                case WorldObjectType.Minion:
                    minions.Remove(unit);
                    break;
            }            
        }

        public void SetSofa(SofaUnit unit)
        {
            sofa = unit;
        }

        
    }

}
