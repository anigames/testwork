﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SofaWarriors {
    public class UIController : MonoBehaviour
    {

#pragma warning disable
        [SerializeField]
        GameObject uiLaunch;
        [SerializeField]
        GameObject uiLoading;
        [SerializeField]
        GameObject uiStarting;
        [SerializeField]
        GameObject uiFreePlay;
        [SerializeField]
        GameObject uiFail;
#pragma warning restore

        Dictionary<GameState, GameObject> uiStates;

        void Awake()
        {
            uiStates = new Dictionary<GameState, GameObject>();
            uiStates.Add(GameState.Launch, uiLaunch);
            uiStates.Add(GameState.Loading, uiLoading);
            uiStates.Add(GameState.Starting, uiStarting);
            uiStates.Add(GameState.FreePlay, uiFreePlay);
            uiStates.Add(GameState.Fail, uiFail);

            GameManager.OnGameStateChange += OnGameStateChange;

            OnGameStateChange(GameState.Launch, GameState.Launch);
        }

        void OnDestroy()
        {
            GameManager.OnGameStateChange -= OnGameStateChange;
        }

        private void OnGameStateChange(GameState prevState, GameState currentState)
        {
            if (!uiStates.ContainsKey(currentState))
            {
                Debug.LogError("UI State " + currentState + " is not exist.");
                return;
            }
            uiStates[prevState].SetActive(false);
            uiStates[currentState].SetActive(true);
        }

    }
}