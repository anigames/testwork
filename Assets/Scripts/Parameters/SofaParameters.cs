﻿using UnityEngine;
using System.Collections;

namespace SofaWarriors
{

    public class SofaParameters : ScriptableObject
    {
#pragma warning disable
        [SerializeField]
        UnitPassport sofaUnitPassport;
#pragma warning restore

        public UnitPassport SofaUnitPassport { get { return sofaUnitPassport; } }
    }
}
