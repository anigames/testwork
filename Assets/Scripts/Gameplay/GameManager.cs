﻿using UnityEngine;
using System.Collections;

namespace SofaWarriors
{

    public class GameManager : MonoBehaviour
    {
#pragma warning disable
        [SerializeField]
        private ObjectFactory factory;
#pragma warning restore
        /// <summary>
        /// Cast event if game state changes from one state to another
        /// </summary>
        public static event System.Action<GameState, GameState> OnGameStateChange;
        public static event System.Action<int> OnStartingTimerTick;

        private GameRound gameRound;
        private GameState state;

        public GameState State
        {
            get
            {
                return state;
            }
            set
            {
                if (value == state)
                    return;

                switch (value)
                {
                    case GameState.Loading:                        
                        break;
                    case GameState.Starting:
                        gameRound = new GameRound(factory);
                        StartCoroutine(StartingTimerTick());
                        break;
                }

                Debug.Log("Change from " + state + " to " + value);

                if (OnGameStateChange != null)
                    OnGameStateChange(state, value);

                state = value;

            }
        }

        public static GameManager Instance { get; private set; }

        void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }

            
        }

        void Start()
        {
            State = GameState.Starting;
        }

        void OnDestroy()
        {
            Instance = null;
        }

        IEnumerator StartingTimerTick()
        {
            int timeLeft = 3;
            while (timeLeft > 0)
            {
                if (OnStartingTimerTick != null)
                    OnStartingTimerTick(timeLeft);
                timeLeft--;
                yield return new WaitForSeconds(1);
            }
            State = GameState.FreePlay;
        }

        public ObjectController GetObjectController()
        {
            return gameRound.GetObjectController();
        }

        public ObjectFactory GetObjectFactory()
        {
            return factory;
        }
    }
}
