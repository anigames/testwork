﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SofaWarriors
{
    public class Building : MonoBehaviour, ISelectable
    {
#pragma warning disable
        [SerializeField]
        GameObject selectionMarker;
#pragma warning restore

        private BuildingPassport buildingPassport;

        public BuildingPassport Passport
        {
            get { return buildingPassport; }
        }

        public int Level { get; private set; }

        public void Initialize(BuildingPassport passport)
        {
            buildingPassport = passport;
        }

        public void Select()
        {
            selectionMarker.SetActive(true);
        }

        public void Unselect()
        {
            selectionMarker.SetActive(false);
        }

        public bool IsSelected()
        {
            return selectionMarker.activeInHierarchy;
        }
    }

}