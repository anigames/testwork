﻿using UnityEngine;
using System.Collections;

namespace SofaWarriors
{
    public class GameRound
    {

        ObjectController objectController;
        ObjectFactory objectFactory;
        BuildingParameters buildingParameters;

        public GameRound(ObjectFactory factory)
        {
            objectFactory = factory;
            objectController = new ObjectController();
            buildingParameters = Resources.Load<BuildingParameters>("building_passports");

            //Все здания кроме Sofa
            BuildingPassport[] passports = buildingParameters.GetAllBildings();

            foreach (BuildingPassport passport in passports)
            {
                CreateBuilding(passport);
            }
            
            BuildingPassport sofaPassport = buildingParameters.Sofa;
            SofaParameters sofaUnitParameters = Resources.Load<SofaParameters>("sofa_unit_passport");
            GameObject sofa = CreateBuilding(sofaPassport).gameObject;
            SofaUnit sofaUnit = sofa.GetComponent<SofaUnit>();
            sofaUnit.Initialize(sofaUnitParameters.SofaUnitPassport,objectController);
            objectController.SetSofa(sofaUnit);

        }

        Building CreateBuilding(BuildingPassport passport)
        {
            Building building = objectFactory.Build(passport.Prefab).GetComponent<Building>();
            building.Initialize(passport);
            objectController.AddBuilding(building);
            return building;
        }

        public ObjectController GetObjectController()
        {
            return objectController;
        }

        
    }
}
