﻿using UnityEngine;
using System.Collections;

namespace SofaWarriors
{

    [System.Serializable]
    public class UnitPassport
    {
#pragma warning disable
        [SerializeField]
        private string name;
        [SerializeField]
        private GameObject prefab;
        [SerializeField]
        private int hp;
        [SerializeField]
        private float armor;
        [SerializeField]
        private int attack;
        [SerializeField]
        private float attackSpeed;
        [SerializeField]
        private float speed;
        [SerializeField]
        private float attackRange;
        [SerializeField]
        private int gold;
        [SerializeField]
        private int xp;
        [SerializeField]
        private WorldObjectType objectType;
#pragma warning restore

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public GameObject Prefab
        {
            get { return prefab; }
            set { prefab = value; }
        }

        public int HP
        {
            get { return hp; }
            set { hp = value; }
        }

        public float Armor
        {
            get { return armor; }
            set { armor = value; }
        }
        public int Attack
        {
            get { return attack; }
            set { attack = value; }
        }

        public float AttackSpeed
        {
            get { return attackSpeed; }
            set { attackSpeed = value; }
        }

        public float Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        public float AttackRange
        {
            get { return attackRange; }
            set { attackRange = value; }
        }

        public int Gold
        {
            get { return gold; }
            set { gold = value; }
        }

        public int XP
        {
            get { return xp; }
            set { xp = value; }
        }

        public WorldObjectType ObjectType
        {
            get { return objectType; }
            set { objectType = value; }
        }
    }
}
