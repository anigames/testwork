﻿using UnityEngine;
using System.Collections;
using System;

namespace SofaWarriors
{

    public class SofaUnit : UnitBase
    {

        public override void SetState(UnitState state)
        {
            if (state == UnitState.Dying)
                GameManager.Instance.State = GameState.Fail;
        }

        public override void UpdateUnit()
        {
            
        }

        protected override void PrepareUnit()
        {
            
        }
    }
}
