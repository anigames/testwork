﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace SofaWarriors
{
    public class UIBuildingInfo : MonoBehaviour
    {
#pragma warning disable
        [SerializeField]
        GameObject panel;
        [SerializeField]
        Image avatar;
        [SerializeField]
        Text title;
        [SerializeField]
        Text level;
        [SerializeField]
        Text upgrade;
        [SerializeField]
        Button btnUpgrade;
        [SerializeField]
        CanvasGroup canvasGroup;
#pragma warning restore

        Coroutine showPanelCorutine;

        void Start()
        {
            canvasGroup.alpha = 0;
            canvasGroup.interactable = false;

            ObjectController.OnBuildingSelect += SelectBuilding;
            ObjectController.OnUnselect += UnselectBuilding;
        }

        void OnDestroy()
        {
            ObjectController.OnBuildingSelect -= SelectBuilding;
            ObjectController.OnUnselect -= UnselectBuilding;
        }

        void SelectBuilding(Building building)
        {
            if (showPanelCorutine!=null)
                StopCoroutine(showPanelCorutine);
            showPanelCorutine = StartCoroutine(ShowPanel(0.05f));
            title.text = building.Passport.Name;
            level.text = (building.Level + 1).ToString() + "/5";
        }

        void UnselectBuilding()
        {
            canvasGroup.alpha = 0;
            canvasGroup.interactable = false;
        }

        IEnumerator ShowPanel(float speed)
        {
            canvasGroup.alpha = 0;
            while (canvasGroup.alpha < 1)
            {
                canvasGroup.alpha += speed;
                yield return new WaitForSeconds(Time.fixedDeltaTime);
            }
            canvasGroup.interactable = true;
        }

    }
}