﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SofaWarriors
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(Collider))]

    public abstract class UnitBase : MonoBehaviour
    {
        //Characteristics
        protected UnitPassport unitPassport;

        public UnitPassport Passport
        {
            get { return unitPassport; }
        }

        public UnitState State { get; protected set; }

        public UnitBase TargetUnit { get; protected set; }        

        public int Health { get; private set; }

        protected ObjectController objectController;
        protected Rigidbody rigid;
        [SerializeField]
        protected Animator animator;

        private int roundDirection = 1;

        public virtual void Initialize(UnitPassport passport, ObjectController objController)
        {
            unitPassport = passport;
            Health = passport.HP;
            objectController = objController;
            PrepareUnit();
        }

        protected abstract void PrepareUnit();

        public abstract void SetState(UnitState state);
        public abstract void UpdateUnit();

        void Awake()
        {
            rigid = GetComponent<Rigidbody>();
        }

        public void FindNearestEnemy()
        {
            TargetUnit = objectController.GetNearestEnemy(this, unitPassport.ObjectType);
        }

        public void Damage(int damage, UnitBase subject)
        {
            if (State == UnitState.Dying)
                return;

            //Аварийное переключение
            TargetUnit = subject;
            State = UnitState.LookingEnemy;

            int damageReal = damage - (int)(damage * unitPassport.Armor);
            Health = Mathf.Clamp(Health - damageReal, 0, unitPassport.HP);
            if (Health == 0)
                SetState(UnitState.Dying);
        }

        protected void MoveTo(Vector3 pos)
        {
            pos.y = this.transform.position.y;
            transform.LookAt(pos,Vector3.up);
            rigid.velocity = this.transform.forward * unitPassport.Speed;
        }

        protected void BackToPool()
        {
            Destroy(gameObject);
        }


    }

}