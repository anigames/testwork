﻿
public enum GameState
{
    Launch,
    Loading,
    Starting,
    FreePlay,
    Fail,
    Win
}

public enum InputMode
{
    Selection,
    Click
}

public enum WorldObjectType
{
    Minion,
    Enemy,
    Building
}

public enum UnitState
{
    Idle,
    LookingEnemy,
    Attack,
    LookingFountain,
    RecoveryHealth,
    Dying
}

public enum UnitType
{
    Warrior,
    Archer,
    Boss,
    Player
}